
name := "Blog-API"
version := "1.0.0"
organization := "Krich\'s Blog"
scalaVersion := "2.12.6"

scalacOptions ++= Seq("-feature")

libraryDependencies ++= {
  val AkkaHttpVersion = "10.0.6"
  val Json4sVersion = "3.5.2"
  Seq(

    "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
    // For the native support
    "org.json4s"        %% "json4s-native"   % Json4sVersion,
    // Support for Enum, Joda-Time, ...
    "org.json4s"        %% "json4s-ext"      % Json4sVersion,
    // Json4s from de.heikoseeberger MVNrepository
    "de.heikoseeberger" %% "akka-http-json4s" % "1.16.0"
  )
}

mainClass in Global := Some("io.swagger.server.Main")

