package io.swagger.server

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Route
import io.swagger.server.api.{DefaultApi, DefaultApiService}

import scala.concurrent.ExecutionContext


trait RestInterface extends DefaultApi {

  implicit def executionContext: ExecutionContext
  implicit  val system: ActorSystem
  lazy val articleService = new DefaultApiService()

  val routes: Route = articleRoutes

}

