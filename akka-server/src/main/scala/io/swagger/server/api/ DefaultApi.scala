package io.swagger.server.api
import akka.actor.ActorSystem
import akka.http.scaladsl.marshalling.{ToResponseMarshallable, ToResponseMarshaller}
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.headers.{Location, _}
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.{Directive0, Directives, Route}
import akka.pattern.ask
import akka.util.Timeout
import io.swagger.server.model.{Article, ArticleUpdate}
import io.swagger.server.serializers.JsonSupport
import krichs.blog.actors._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps



trait CORSHandler{

  private val corsResponseHeaders = List(
    `Access-Control-Allow-Origin`.*,
    `Access-Control-Allow-Credentials`(true),
    `Access-Control-Allow-Headers`("Content-Type" ,"X-Requested-With", "Authorization"),
    `Access-Control-Max-Age`(1.day.toMillis) //Tell browser to cache OPTIONS requests
  )

  //this directive adds access control headers to normal responses
   def addAccessControlHeaders: Directive0 = {
    respondWithHeaders(corsResponseHeaders)
  }

  //this handles preflight OPTIONS requests.
   private  def preflightRequestHandler: Route = options {
      complete(HttpResponse(StatusCodes.OK).
      withHeaders(`Access-Control-Allow-Methods`(OPTIONS,POST, PUT, GET, DELETE)))
  }

  // Wrap the Route with this method to enable adding of CORS headers 
  def corsHandler(r: Route): Route = addAccessControlHeaders {
    preflightRequestHandler ~ r
  }

  // Helper method to add CORS headers to HttpResponse 
  // preventing duplication of CORS headers across code
  def addCORSHeaders(response: HttpResponse):HttpResponse =
    response.withHeaders(corsResponseHeaders)

}



/*
 *  here i will define my routes and complete my futures
 *  that comes from my services
 *  My resource is where is defined Marshalling methods
 */

trait DefaultApi extends DefaultApiMarshaller {

  private val cors = new CORSHandler {}

  val articleService: DefaultApiService

  def articleRoutes: Route = pathPrefix("articles") {
    pathEnd {
      options {
        cors.corsHandler(complete(StatusCodes.OK))
      } ~
      post {
        entity(as[Article]) { article =>
          cors.corsHandler(completeWithLocationHeader(
            resourceId = articleService.createArticle(article),
            ifDefinedStatus = 200, ifEmptyStatus = 404))
        }
      }
    } ~
    path(IntNumber) { id =>
      get {
        cors.corsHandler(
        complete(articleService.getArticle(id)))

      }~ options {
        cors.corsHandler(complete(StatusCodes.OK))
      } ~
      put {
        entity(as[ArticleUpdate]) { update =>
          cors.corsHandler(complete(articleService.updateArticle(id, update)))
        }
      } ~ options {
        cors.corsHandler(complete(StatusCodes.OK))
      } ~
      delete {
        cors.corsHandler(complete(articleService.deleteArticle(id)))
      }
    } ~
    path("stopWorking") {
      get {

        // not needed in swagger but we can also add it if we want
        // not useful cors.corsHandler here

        complete(articleService.stopWorking())
      }
    }
  }
}

class DefaultApiService(implicit val executionContext: ExecutionContext , implicit val system : ActorSystem) {

  // this articles Vector was used before actors now collection of articles is in actors class
  // var articles = Vector.empty[Article]

  val blogActors = new BlogActors(system)
  implicit val timeout = Timeout(5 seconds)

  def createArticle(article: Article) : Future[Option[Int]] = {

    /*
     * ask pattern returns by default Futur[Any] using mapTO to get Futur[Option[Int]]
     * lot of time spent on akka docs to understand this thing !!
     */

   (blogActors.versatileEmployee ? createArticleVersatileEmployee(article)).mapTo[Option[Int]]

  }

  def getArticle(id: Int): Future[Option[Article]]  = {

    (blogActors.versatileEmployee ? getArticleVersatileEmployee(id)).mapTo[Option[Article]]


  }

  /*
    * here i does not really need to ask my actor to do the job because
    * i'm just using the methods already done by the actor
   */

  def updateArticle(id: Int, update: ArticleUpdate): Future[Option[Article]] = {

    def updateEntity(article: Article): Article = {

      val articlename = update.articlename.getOrElse(article.articlename)
      val articletitle = update.articletitle.getOrElse(article.articletitle)
      val articleconetent = update.articleconetent.getOrElse(article.articleconetent)
      val articlecategory = update.articlecategory.getOrElse(article.articlecategory)


      // save created date because it will never change
      
      val createdAt = article.createdAt
      val art = Article(id, articlename , articletitle ,articleconetent , articlecategory)

      // update date here 

      art.updatedAt = Some(new java.util.Date)

      // retreive date of first creation

      art.createdAt = createdAt

      art
    
    }

    getArticle(id).flatMap { maybeArticle =>
      maybeArticle match {
        case None => Future { None } // No article found, nothing to update
        case Some(article) =>
          val updatedArticle = updateEntity(article)
          deleteArticle(id).flatMap { _ =>
            createArticle(updatedArticle).map(_ => Some(updatedArticle))
          }
      }
    }
  }

  def deleteArticle(id : Int) : Future[Unit] = {

    // asking versatileEmployee actor to delete some article

    (blogActors.versatileEmployee ? deleteArticleVersatileEmployee(id)).mapTo[Unit]

  }


 /* i'm telling my versatileEmployee to stop working
  * the Manager actor takes care of stopping it and keeps in memory the last sender
  * and finally answer to service
*/

  def stopWorking() : Future[Option[String]] = {

    (blogActors.manager ? "stop").mapTo[Option[String]]

  }

}


// Marshalling and JsonSupport

trait DefaultApiMarshaller extends Directives with JsonSupport {

  implicit def executionContext: ExecutionContext

  def completeWithLocationHeader[T](resourceId: Future[Option[T]], ifDefinedStatus: Int, ifEmptyStatus: Int): Route =
    onSuccess(resourceId) {
      case Some(t) => completeWithLocationHeader(ifDefinedStatus, t)
      case None => complete(ifEmptyStatus, None)
    }

  def completeWithLocationHeader[T](status: Int, resourceId: T): Route =
    extractRequestContext { requestContext =>
      val request = requestContext.request
      val location = request.uri.copy(path = request.uri.path / resourceId.toString)
      respondWithHeader(Location(location)) {
        complete(status, None)
      }
    }

  def complete[T: ToResponseMarshaller](resource: Future[Option[T]]): Route =
    onSuccess(resource) {
      case Some(t) => complete(ToResponseMarshallable(t))
      case None => complete(404, None)
    }

  def complete(resource: Future[Unit]): Route = onSuccess(resource) { complete(200, None) }

}



