package io.swagger.server.model

import java.util._

/**
 * Model contaning article info
 *
 * @param id  for example: ''4''
 * @param articlename  for example: ''krichs hobies''
 * @param articletitle  for example: ''this is an article''
 * @param articleconetent  for example: ''welcome to my blog my hobies is playing video games''
 * @param articlecategory  for example: ''Other''
 */
case class Article (
                     id: Int,
                     articlename: String,
                     articletitle: String,
                     articleconetent: String,
                     articlecategory: String,
                     var createdAt : Date = new Date,
                     var updatedAt : Option[Date] = None
                   )