package krichs.blog.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated}
import io.swagger.server.model.Article

  case class deleteArticleVersatileEmployee(id: Int)

  case class getArticleVersatileEmployee(id: Int)

  case class createArticleVersatileEmployee(article: Article)


// versatileEmployee the century scam ! 
// it seems fun since i have one and only actor that do every thing


class VersatileEmployee extends Actor  with ActorLogging{

  def apply(): Props = Props(new VersatileEmployee())

  var articles = Vector.empty[Article]
  var salary : Int = -200

  // i'm just playing with actors life cycle
  // it is definitely useless at this point 

  // this is what to do when just starting

    override def preStart() = {
      super.preStart()
     println(s"${self.path.name} : start working")
     salary+=10
     println(s"${self.path.name} : current salary :"+salary)
    
    }

  override def receive: Receive =  {

    case deleteArticleVersatileEmployee(id) =>

      println(s"${self.path.name} : is deleting article with id : "+id)
      sender() ! (articles = articles.filterNot(_.id == id))
      salary+=10
      println(s"${self.path.name} : current salary :"+salary)

    case  getArticleVersatileEmployee(id) =>
      // the print is just a tracking of my actor
     
     println(s"${self.path.name} : is getting an article with id : "+id)
     sender() !  (articles.find(_.id == id))
     salary+=10
     println(s"${self.path.name} : current salary :"+salary)

    
    case createArticleVersatileEmployee(article) =>
      
      // the print is just a tracking of my actor
      println(s"${self.path.name} : is creating a new Article")
      sender() ! (
        articles.find(_.id == article.id) match {
        case Some(q) => None // Conflict! id is already taken
        case None =>

          articles = articles :+ article
          Some(article.id)
      }
    )
      salary+=10
      println(s"${self.path.name} : current salary :"+salary)

      
  }
   
   // this will happen when actor finish to receive messages
   // it is definitely useless at this point 

    override def postStop()  = {
      super.postStop()
     println(s"${self.path.name} : finished working")
      if(salary >= 10) {
        println(s"${self.path.name} : Thank you Sir ! Now i will probably survive in such a wild capitalism ! :)")
      }
      else {
        println(s"${self.path.name} : Oh No ! Please Sir ! let me work a little bit more .. i have bills to pay ! ;(")

      }

    }


}


case object Manager {

def apply(verEmployee: ActorRef , sys : ActorSystem): Props = Props(new Manager(verEmployee ,sys))

}

/*
  ** here i created a new actor Manager that can stop the VersatileEmployee
  ** because i can't realy ask the actor to stop hem self because he will stop
  ** and never answer me ! i tried to do that  

*/

class Manager(verEmployee : ActorRef , system : ActorSystem) extends Actor with ActorLogging {


  context.watch(verEmployee)
  var lastSender = context.system.deadLetters

override def receive: Receive =  {
  case "stop" =>
	    context.stop(verEmployee)
	    lastSender = sender()
	case Terminated(`verEmployee`) =>
	    lastSender ! Some("versatileEmployee finished working success !")
	    system.terminate()
  }
}


class BlogActors(system : ActorSystem) {


  val versatileEmployee = system.actorOf(Props[VersatileEmployee] , "Employee")
  val manager = system.actorOf(Manager(versatileEmployee ,system), name = "Manager")


}
