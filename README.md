
<h2>akka-http SAMPLE API for krich-s-blog using swagger and openapi 3</h2>

## How to run the service

> Clone the repository:

`git clone https://gitlab.mipn.fr/KrichAyyoub/swagger-generated-blog-api-akka-http.git`

Get to the `akka-server` folder:

`cd swagger-generated-blog-api-akka-http && cd akka-server`

> Run the service:

`sbt run`

The service runs on port 8081 by default.

## usage with swagger-editor 

`fire a new terminal`

`sudo docker pull swaggerapi/swagger-editor`

<p> this will download swagger-editor docker image from docker hub </p>

`sudo docker run -d -p 8080:8080 swaggerapi/swagger-editor`

<p> and this will bind swagger-editor in http://localhost:8080 </p>

`go to https://gitlab.mipn.fr/KrichAyyoub/swagger-generated-blog-api-akka-http/raw/master/openapi3.yaml`

`copy and past in swagger-editor`


## Usage with curl command line

Article entity:
`case class Article(id: Int, articlename: String, articletitle:String ,articleconetent : String , articlecategory  : String , var createdAt : Date = new Date , var updated : Option[Date] = None )`

### POST an article

> Request:


`POST request format is important !`

```

curl -v -H "Content-Type: application/json" \
         -X POST http://localhost:8081/articles \
         -d '{"id": 1, "articlename": "article name", "articletitle":"The title of my article" , "articleconetent": "article content" ,  "articlecategory": "article category"}'

```

> Response if the article has been created:

`Trying ::1...
TCP_NODELAY set
Connected to localhost (::1) port 8081 (#0)
POST /articles HTTP/1.1
Host: localhost:8081
User-Agent: curl/7.52.1
Accept: */*
Content-Type: application/json
Content-Length: 162
upload completely sent off: 162 out of 162 bytes
HTTP/1.1 200 OK
Location: http://localhost:8081/articles/1
Server: akka-http/10.0.6
Date: Thu, 13 Feb 2020 02:34:03 GMT
Content-Type: application/json
Content-Length: 0
Curl_http_done: called premature == 0
Connection #0 to host localhost left intact`



> Response if the article with the specified id already exists:

`Trying ::1...
TCP_NODELAY set
Connected to localhost (::1) port 8081 (#0)
POST /articles HTTP/1.1
Host: localhost:8081
User-Agent: curl/7.52.1
Accept: */*
Content-Type: application/json
Content-Length: 162
upload completely sent off: 162 out of 162 bytes
HTTP/1.1 404 Not Found
Server: akka-http/10.0.6
Date: Thu, 13 Feb 2020 02:34:37 GMT
Content-Type: application/json
Content-Length: 0
Curl_http_done: called premature == 0
Connection #0 to host localhost left intact`

### Get an article

> Request:

`http localhost:8081/articles/1`

> Response if the article exists:

`HTTP/1.1 200 OK
Content-Length: 185
Content-Type: application/json
Date: Thu, 13 Feb 2020 02:35:31 GMT
Server: akka-http/10.0.6
{
    "articlecategory": "article category", 
    "articleconetent": "article content", 
    "articlename": "article name", 
    "articletitle": "The title of my article", 
    "createdAt": "13/02/2020 03:34:02", 
    "id": 1
}`

> Response if the article does not exist:

`HTTP/1.1 404 Not Found
Content-Length: 0
Content-Type: application/json
Date: Thu, 13 Feb 2020 02:37:00 GMT
Server: akka-http/10.0.6`


### Update an article

> Request:



```

curl -v -H "Content-Type: application/json" \
	 -X PUT http://localhost:8081/articles/1 \
	 -d '{"articlename":"Another name"}'

```

> Response if the article has been updated:

`Trying ::1...
TCP_NODELAY set
Connected to localhost (::1) port 8081 (#0)
PUT /articles/1 HTTP/1.1
Host: localhost:8081
User-Agent: curl/7.52.1
Accept: */*
Content-Type: application/json
Content-Length: 30
upload completely sent off: 30 out of 30 bytes
HTTP/1.1 200 OK
Server: akka-http/10.0.6
Date: Thu, 13 Feb 2020 02:37:53 GMT
Content-Type: application/json
Content-Length: 219
Curl_http_done: called premature == 0
Connection #0 to host localhost left intact
{"id":1,"articlename":"Another name","articletitle":"The title of my article","articleconetent":"article content","articlecategory":"article category","createdAt":"13/02/2020 03:34:02","updatedAt":"13/02/2020 03:37:53"}%`


<!-- this is working in github but not working good in gitlab !-->
<!--![Screenshot](img/6.png) !-->

> Response if the article could not be updated:

` Trying ::1...
TCP_NODELAY set
Connected to localhost (::1) port 8081 (#0)
PUT /articles/2 HTTP/1.1
Host: localhost:8081
User-Agent: curl/7.52.1
Accept: */*
Content-Type: application/json
Content-Length: 30
upload completely sent off: 30 out of 30 bytes
HTTP/1.1 404 Not Found
Server: akka-http/10.0.6
Date: Thu, 13 Feb 2020 02:38:44 GMT
Content-Type: application/json
Content-Length: 0
Curl_http_done: called premature == 0
Connection #0 to host localhost left intact `

### Delete an article

> Request:

`curl -v -X DELETE http://localhost:8081/articles/1`

> Response:

`Trying ::1...
TCP_NODELAY set
Connected to localhost (::1) port 8081 (#0)
DELETE /articles/1 HTTP/1.1
Host: localhost:8081
User-Agent: curl/7.52.1
Accept: */* 
HTTP/1.1 200 OK
Server: akka-http/10.0.6
Date: Thu, 13 Feb 2020 02:40:03 GMT
Content-Type: application/json
Content-Length: 0
Curl_http_done: called premature == 0
Connection #0 to host localhost left intact`

### Stop the Versatile Employee (the Main Actor)

> Request:

`curl -i  http://localhost:8081/articles/stopWorking`

> Response:

`HTTP/1.1 200 OK
Content-Length: 46
Content-Type: application/json
Date: Thu, 13 Feb 2020 02:41:32 GMT
Server: akka-http/10.0.6
"versatileEmployee finished working success !"`

> Notice that when we stop the Main actor we exit the server 

`[success] Total time: 537 s (08:57), completed 13 févr. 2020 03:41:32`
